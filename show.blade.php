@extends ('index')

@section('head.title')

Tiêu đề bài viết (trên thanh tab)

@stop

@section('body.content')
<div class="container" style = "margin-bottom: 30px">
<div class="row">
	<div class="col-sm-6 col-sm-offset-2">
		<a href="{{ url( '/' )}}" class="btn btn-link">
			<span class="glyphicon glyphicon-home"></span>
			Back to home
		</a>
	</div>
</div>

<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<h2>{{$article->title}}</h2>
		<p> {{ $article-> content}} </p>
		
	</div>
</div>		

</div>
@stop